package main.java;

public class FactTextRequest {
    private int id;

    public FactTextRequest(int id) {
        this.id = id;
    }

    public FactTextRequest() {
    }

    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "FactTextRequest{" +
                "id=" + id +
                '}';
    }


}
