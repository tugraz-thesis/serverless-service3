package main.java;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Random;

public class Service3Handler implements RequestHandler<FactTextRequest, FactTextResponse> {

    // reuse db connection from container outside of handler method
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();

    public FactTextResponse handleRequest(FactTextRequest factTextRequest, Context context) {

        AlexaFact alexaFact;
        FactTextResponse factTextResponse;
        alexaFact = session.find(AlexaFact.class,factTextRequest.getId());

        if(alexaFact != null) {
            factTextResponse = new FactTextResponse(alexaFact.getFactText());

        }else {
            factTextResponse = new FactTextResponse("");
        }

        return factTextResponse;

    }

}